> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4368

## Kristopher Mangahas

### Assignment 3 



> #### Deliverables

1. Entity Relationship Diagram (ERD)
2. Include data(at least 10 records each tables)
3. Provide Bitbucket read only access to a3 repo (Language SQL), *must* include README.md,
using Markdown Syntax, and include links to *all* of the following files (From README.md):
	
	a.docs folder: a3.mwb, and a3.sql
	
	b.img folder: a3.png (export a3.mwb file as a3.png
	
	c.README.MD (*MUST* display a3.png ERD)

4. Blackboard Links: a3 Bitbucket Repo
 


#### Assignment Documents:

*a3.mwb*

[MWB of a3 ERD](docs/a3.mwb)

*a3.sql*

[sql statements of of a3 ERD](docs/a3.sql)

#### Assignment Screenshots:

*a3.png (PNG of a3 ERD)*:

![PNG of a3 ERD](img/a3.png)

